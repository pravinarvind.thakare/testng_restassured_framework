package Test_Case_Pojo;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Pojos.Pojo_Post_API;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class TestNG_Annotation_Pojo_Post_TC {

	String requestBody;
	String Endpoint;
	File dir_name;
	Response response;

	ObjectMapper objMapper;
	Pojo_Post_API objPojo;

	@BeforeTest
	public void testSteUp() throws IOException {

		objMapper = new ObjectMapper();
		objPojo = new Pojo_Post_API();

		objPojo.setName("Roman");
		objPojo.setJob("SrLead");

		requestBody = objMapper.writeValueAsString(objPojo);

		System.out.println("Before Test Method Called");
		dir_name = Utility.CreateLogDirectory("Post_API_logs"); // create the log directory
		// requestBody = RequestBody.req_post_tc("Post_TC1"); // fetch the request body
		// and save it here
		Endpoint = RequestBody.Hostname() + RequestBody.Resource(); // fetch the endpoint and save it here
	}

	@Test(description = "Validate the response body parameters of Post API Test_Case_1")
	public void validator() {

		System.out.println("Test Method Called");
		response = API_Trigger.Post_trigger(RequestBody.HeaderName(), RequestBody.HeaderValue(), requestBody, Endpoint);

		int statuscode = response.statusCode();

		ResponseBody res_body = response.getBody();
		String res_name = res_body.jsonPath().getString("name");
		String res_job = res_body.jsonPath().getString("job");
		String res_id = res_body.jsonPath().getString("id");
		String res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);

		// Set the expected results
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Validate the response parameters
		Assert.assertEquals(response.statusCode(), 201);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);

	}

	@AfterTest
	public void evidenceCreater() throws IOException {

		System.out.println("After Test Method Called");
		Utility.evidenceFileCreater(Utility.testLogName("Test_Case_1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
	}

}
