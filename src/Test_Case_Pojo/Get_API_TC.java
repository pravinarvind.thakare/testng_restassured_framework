package Test_Case_Pojo;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Pojos.Pojo_Get_API_Parrent;
import Repository.Environment;
import Repository.RequestBody;

import io.restassured.response.Response;

public class Get_API_TC {

	File dir_name;
	String Endpoint;
	Response response;

	@BeforeTest
	public void testSetup() {

		dir_name = Utility.CreateLogDirectory("Get_API_logs");
		//Endpoint = RequestBody.Hostname() + RequestBody.Resource_Get();
		Endpoint = Environment.Hostname() + Environment.Resource_Get();
		
	}

	@Test(description = "Fetch/Validate the Get Api Response")
	public void validator() {

		response = API_Trigger.Get_trigger(Endpoint);

		int statuscode = response.statusCode();

		Pojo_Get_API_Parrent res_body = response.as(Pojo_Get_API_Parrent.class);

		// Set Expected Result
		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;
		String exp_support_url = "https://reqres.in/#support-heading";
		String exp_support_text = "To keep ReqRes free, contributions towards server costs are appreciated!";

		// set expected result for "data"
		int exp_id[] = { 7, 8, 9, 10, 11, 12 };
		String exp_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String exp_first_name[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String exp_last_name[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String exp_avatar[] = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };

		// fetch response body parameters
		int res_page = res_body.getPage();
		
		int res_per_page = res_body.getPer_page();
		
		int res_total = res_body.getTotal();
		
		int res_total_pages = res_body.getTotal_pages();
		
		String res_support_text = res_body.getSupport().getText();
		
		String res_support_url = res_body.getSupport().getUrl();
		
		// Validate Response Body Parameters
		
		  Assert.assertEquals(statuscode, 200); 
		  Assert.assertEquals(res_page,exp_page);
		  Assert.assertEquals(res_per_page, exp_per_page);
		  Assert.assertEquals(res_total, exp_total);
		  Assert.assertEquals(res_total_pages, exp_total_pages);
		  Assert.assertEquals(res_support_text, exp_support_text);
		  Assert.assertEquals(res_support_url, exp_support_url);
		  System.out.println("Get API Successfully Validated..");
		 

		// get count for "data" parameters
		int res_dataarray_size = res_body.getData().size();
		
		// declare array for "data" values
		int res_id_arr[] = new int[6]; 
		String res_email_arr[] = new String[6];
		String res_firstname_arr[] = new String[6];
		String res_lastname_arr[] = new String[6];
		String res_avatar_arr[] = new String[6];

		// fetch response body "Data" parameters
		for (int i = 0; i < res_dataarray_size; i++) {

			int res_data_id = res_body.getData().get(i).getId();
			res_id_arr[i] = res_data_id;

			String res_data_email = res_body.getData().get(i).getEmail();
			res_email_arr[i] = res_data_email;

			String res_data_firstname = res_body.getData().get(i).getFirst_name();
			res_firstname_arr[i] = res_data_firstname;

			String res_data_lastname = res_body.getData().get(i).getLast_name();
			res_lastname_arr[i] = res_data_lastname;

			String res_data_avatar = res_body.getData().get(i).getAvatar();
			res_avatar_arr[i] = res_data_avatar;

			// validate response body "data" parameters
			Assert.assertEquals(res_id_arr[i], exp_id[i]);
			Assert.assertEquals(res_email_arr[i], exp_email[i]);
			Assert.assertEquals(res_firstname_arr[i], exp_first_name[i]);
			Assert.assertEquals(res_lastname_arr[i], exp_last_name[i]);
			Assert.assertEquals(res_avatar_arr[i], exp_avatar[i]);

		}
		System.out.println("Get API response body Data parameter successully validated");
	}

	@AfterTest
	public void evidenceCreater() throws IOException {

		//System.out.println("After Test Method Called");
		Utility.evidenceFileCreater(Utility.testLogName("TC_Get_Logs_1"), dir_name, Endpoint, null,
				response.getHeader("Date"), response.asPrettyString());
	}

}
 