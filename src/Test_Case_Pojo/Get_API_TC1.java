package Test_Case_Pojo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Pojos.Pojo_Get_API_Parrent;
import Repository.Environment;
import io.restassured.response.Response;

public class Get_API_TC1 {

	String Endpoint;
	File dir_name;
	Response response;

	@BeforeTest
	public void testSetUp() {

		Endpoint = Environment.Hostname() + Environment.Resource_Get();
		dir_name = Utility.CreateLogDirectory("Get_API_logs");
	}

	@Test(description = "Fetch Get API data from Excel and Validate the Get Api Response")
	public void validator() throws IOException {

		response = API_Trigger.Get_trigger(Endpoint);

		Pojo_Get_API_Parrent res_body = response.as(Pojo_Get_API_Parrent.class);

		int count = res_body.getData().size();

		ArrayList<String> exp_id = Utility.readExcelDataForGet("Get_API", "Id");
		ArrayList<String> exp_email = Utility.readExcelDataForGet("Get_API", "Email");
		ArrayList<String> exp_firstname = Utility.readExcelDataForGet("Get_API", "FirstName");
		ArrayList<String> exp_lasttname = Utility.readExcelDataForGet("Get_API", "LastName");
		ArrayList<String> exp_avatar = Utility.readExcelDataForGet("Get_API", "Avatar");

		for (int i = 0; i < count; i++) {

			int res_id = res_body.getData().get(i).getId();
			String res_email = res_body.getData().get(i).getEmail();
			String res_firstname = res_body.getData().get(i).getFirst_name();
			String res_lastname = res_body.getData().get(i).getLast_name();
			String res_avatar = res_body.getData().get(i).getAvatar();

			Assert.assertEquals(Integer.parseInt(exp_id.get(i)), res_id);
			Assert.assertEquals(exp_email.get(i), res_email);
			Assert.assertEquals(exp_firstname.get(i), res_firstname);
			Assert.assertEquals(exp_lasttname.get(i), res_lastname);
			Assert.assertEquals(exp_avatar.get(i), res_avatar);
		}

	}

	@AfterTest
	public void evidenceCreater() throws IOException {

		Utility.evidenceFileCreater(Utility.testLogName("TC_Get_Logs_2"), dir_name, Endpoint, "",
				response.header("Date"), response.asPrettyString());
	}

}
