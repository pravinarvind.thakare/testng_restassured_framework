package Pojos;

import java.util.List;

public class Pojo_Get_API_Support_Child {

	private String url;
	private String text;

	public String getUrl() {
		return url;
	}

	public String getText() {
		return text;
	}

}

// We need to create Pojos for the every json, as and the variable should be as per the keys of that json 
// for example Get API : Parrent pojo, Child Pojo

//Once all pojos are created , we need to define whose child for whom (By defining the return type as class names)
// 1st go the parrent pojo and change the child varaibale data type for json array --> " private List<Pojo_Get_API_Data_Child> data;" 
// 2nd go the parrent pojo and change the child varaibale data type for normal json ---> private Pojo_Get_API_Support_Child support;