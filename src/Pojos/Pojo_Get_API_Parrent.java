package Pojos;

import java.util.List;

public class Pojo_Get_API_Parrent {

	private int page;
	private int per_page;
	private int total;
	private int total_pages;
	private List<Pojo_Get_API_Data_Child> data;
	private Pojo_Get_API_Support_Child support;

	// We generate only Getters because here we are only extracting data . We are 
	// not setting any Request Body

	public int getPage() {
		return page;
	}

	public int getPer_page() {
		return per_page;
	}

	public int getTotal() {
		return total;
	}

	public int getTotal_pages() {
		return total_pages;
	}

	public List<Pojo_Get_API_Data_Child> getData() {
		return data;
	}

	public Pojo_Get_API_Support_Child getSupport() {
		return support;
	}

}
