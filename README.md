# TestNG_RestAssured_Framework

Implented test cases using RestAssured And TestNG Framework and Generated Allure Report.
Executed CRUD API

##Technologies Used
Java
RestAssured Framework
TestNG Framework

#Instructions on how to set up the project for testing.
Create Java Project
Configure Java Project with Maven
Add dependencies in POM.xml File
*dependencies
RestAssured
TestNG
ApachePoi
Allure TestNG

#Sites
Above Framework is implemented by using https://reqres.in/
