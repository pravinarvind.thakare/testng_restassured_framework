Endpoint is :
https://reqres.in/api/users

Request Body is :
{
    "name": "amol",
    "job": "qalead"
}

Response Date is :
Fri, 24 May 2024 08:58:26 GMT

Response Body is :
{"name":"amol","job":"qalead","id":"59","createdAt":"2024-05-24T08:58:26.909Z"}

