Endpoint is :
https://reqres.in/api/users

Request Body is :
{"name":"Roman","job":"SrLead","id":null,"createdAt":null}

Response Date is :
Tue, 19 Mar 2024 15:49:47 GMT

Response Body is :
{"name":"Roman","job":"SrLead","id":"109","createdAt":"2024-03-19T15:49:47.857Z"}

