Endpoint is :
https://reqres.in/api/users

Request Body is :
{
    "name": "pravin",
    "job": "qa"
}

Response Date is :
Sat, 09 Mar 2024 16:54:31 GMT

Response Body is :
{"name":"pravin","job":"qa","id":"85","createdAt":"2024-03-09T16:54:30.946Z"}

