Endpoint is :
https://reqres.in/api/users

Request Body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response Date is :
Fri, 24 May 2024 08:58:23 GMT

Response Body is :
{"name":"morpheus","job":"leader","id":"99","createdAt":"2024-05-24T08:58:23.157Z"}

