Endpoint is :
https://reqres.in/api/users

Request Body is :
{
    "name": "brock",
    "job": "qa"
}

Response Date is :
Wed, 06 Mar 2024 18:14:44 GMT

Response Body is :
{"name":"brock","job":"qa","id":"5","createdAt":"2024-03-06T18:14:44.366Z"}

