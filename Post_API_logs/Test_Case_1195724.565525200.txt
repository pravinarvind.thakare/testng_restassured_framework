Endpoint is :
https://reqres.in/api/users

Request Body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response Date is :
Tue, 12 Mar 2024 14:27:22 GMT

Response Body is :
{"name":"morpheus","job":"leader","id":"477","createdAt":"2024-03-12T14:27:22.844Z"}

